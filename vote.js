const Koa = require('koa')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')
const cors = require('@koa/cors')


const officeRouter = require('./router/office-router')
const partyRouter = require('./router/party-router')
const candidateRouter = require('./router/candidate-router')
const constituencyRouter = require('./router/constituency-router')
const voterRouter = require('./router/voter-router')
const wardsRouter = require('./router/wards-router')
const townRouter = require('./router/town-router')
const villageRouter = require('./router/village-router')
const villagetractsRouter = require('./router/villagetracts-router')
const stateRouter = require('./router/state-router')
const discrictRouter = require('./router/district-router')
const townshipRouter = require('./router/township-router')

const vote = new Koa()
const router = new Router()

vote.use(cors());
vote.use(bodyParser());
vote.use(officeRouter.routes()).use(router.allowedMethods())
vote.use(partyRouter.routes()).use(router.allowedMethods())
vote.use(candidateRouter.routes()).use(router.allowedMethods())
vote.use(constituencyRouter.routes()).use(router.allowedMethods())
vote.use(voterRouter.routes()).use(router.allowedMethods())
vote.use(wardsRouter.routes()).use(router.allowedMethods())
vote.use(townRouter.routes()).use(router.allowedMethods())
vote.use(villageRouter.routes()).use(router.allowedMethods())
vote.use(villagetractsRouter.routes()).use(router.allowedMethods())
vote.use(stateRouter.routes()).use(router.allowedMethods())
vote.use(discrictRouter.routes()).use(router.allowedMethods())
vote.use(townshipRouter.routes()).use(router.allowedMethods())
vote.listen(5000, () => {
    console.log("Server is running at 5000.")
})