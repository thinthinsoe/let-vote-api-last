const config = require('../knexfile')
const knex = require('knex')(config)

const getById = (id) => {
    return knex('candidate').where('candidate_id', id).first()
}

const getAll = () => {
    return knex.select('*').from('candidate')
}
const updateCandidate = (id, values) => {
    return knex('candidate').where('candidate_id', id).update(values).then(() => {
        return getById(id)
    })
}
const createCandidate = (values) => {
    return knex('candidate').insert(values).then((result) => {
        return getById(result[0])
    })
}
const deleteCandidate = (id) => {
    return knex('candidate').where('candidate_id', id).del()
}
module.exports = {
    getById,
    getAll,
    updateCandidate,
    createCandidate,
    deleteCandidate
}