const config = require('../knexfile')
const knex = require('knex')(config)

const getById = (id) => {
    return knex('town').where('town_id', id).first()
}

const getAll = () => {
    return knex.select('*').from('town')
}
const updateTown = (id, values) => {
    return knex('town').where('town_id', id).update(values).then(() => {
        return getById(id)
    })
}
const createTown = (values) => {
    return knex('town').insert(values).then((result) => {
        return getById(result[0])
    })
}
const deleteTown = (id) => {
    return knex('town').where('town_id', id).del()
}
module.exports = {
    getById,
    getAll,
    updateTown,
    createTown,
    deleteTown
}