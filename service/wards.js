const config = require('../knexfile')
const knex = require('knex')(config)

const getById = (id) => {
    return knex('wards').where('wards_id', id).first()
}

const getAll = () => {
    return knex.select('*').from('wards')
}
const updateWards = (id, values) => {
    return knex('wards').where('wards_id', id).update(values).then(() => {
        return getById(id)
    })
}
const createWards = (values) => {
    return knex('wards').insert(values).then((result) => {
        return getById(result[0])
    })
}
const deleteWards = (id) => {
    return knex('wards').where('wards_id', id).del()
}
module.exports = {
    getById,
    getAll,
    updateWards,
    createWards,
    deleteWards
}