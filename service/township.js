const config = require('../knexfile')
const knex = require('knex')(config)

const getById = (id) => {
    return knex('township').where('township_id', id).first()
}

const getAll = () => {
    return knex.select('*').from('township')
}
const updateTownship = (id, values) => {
    return knex('township').where('township_id', id).update(values).then(() => {
        return getById(id)
    })
}
const createTownship = (values) => {
    return knex('township').insert(values).then((result) => {
        return getById(result[0])
    })
}
const deleteTownship = (id) => {
    return knex('township').where('township_id', id).del()
}
module.exports = {
    getById,
    getAll,
    updateTownship,
    createTownship,
    deleteTownship
}