const config = require('../knexfile')
const knex = require('knex')(config)

const getById = (id) => {
    return knex('state').where({'state_id': id, 'status': 'Active'}).first()
}

const getAll = () => {
    return knex.select('*').from('state').where('status', 'Active').orderBy('state_id', 'desc')
}
const updateState = (id, values) => {
    return knex('state').where('state_id', id).update(values).then(() => {
        return getById(id)
    })
}
const createState = (values) => {
    return knex('state').insert(values).then((result) => {
        return getById(result[0])
    })
}

const deleteState = (id) => {
    return knex('state').where('state_id', id).update({status: 'Delete'})
}
module.exports = {
    getById,
    getAll,
    updateState,
    createState,
    deleteState
}