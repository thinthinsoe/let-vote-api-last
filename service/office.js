const config = require('../knexfile')
const knex = require('knex')(config)

const getById = (id) => {
    return knex('office').where('office_id', id).first()
}

const getAll = () => {
    return knex.select('*').from('office')
}
const updateOffice = (id, values) => {
    return knex('office').where('office_id', id).update(values).then(() => {
        return getById(id)
    })
}
const createOffice = (values) => {
    return knex('office').insert(values).then((result) => {
        return getById(result[0])
    })
}
const deleteOffice = (id) => {
    return knex('office').where('office_id', id).del()
}
module.exports = {
    getById,
    getAll,
    updateOffice,
    createOffice,
    deleteOffice
}