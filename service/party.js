const config = require('../knexfile')
const knex = require('knex')(config)

const getById = (id) => {
    return knex('party').where('party_id', id).first()
}

const getAll = () => {
    return knex.select('*').from('party')
}
const updateParty = (id, values) => {
    return knex('party').where('party_id', id).update(values).then(() => {
        return getById(id)
    })
}
const createParty = (values) => {
    return knex('party').insert(values).then((result) => {
        return getById(result[0])
    })
}
const deleteParty = (id) => {
    return knex('party').where('party_id', id).del()
}
module.exports = {
    getById,
    getAll,
    updateParty,
    createParty,
    deleteParty
}