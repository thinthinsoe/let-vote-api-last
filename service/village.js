const config = require('../knexfile')
const knex = require('knex')(config)

const getById = (id) => {
    return knex('village').where('village_id', id).first()
}

const getAll = () => {
    return knex.select('*').from('village')
}
const updateVillage = (id, values) => {
    return knex('village').where('village_id', id).update(values).then(() => {
        return getById(id)
    })
}
const createVillage = (values) => {
    return knex('village').insert(values).then((result) => {
        return getById(result[0])
    })
}
const deleteVillage = (id) => {
    return knex('village').where('village_id', id).del()
}
module.exports = {
    getById,
    getAll,
    updateVillage,
    createVillage,
    deleteVillage
}