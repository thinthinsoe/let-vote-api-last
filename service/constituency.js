const config = require('../knexfile')
const knex = require('knex')(config)

const getById = (id) => {
    return knex('constituency').where('constituency_id', id).first()
}

const getAll = () => {
    return knex.select('*').from('constituency')
}
const updateConstituency = (id, values) => {
    return knex('constituency').where('constituency_id', id).update(values).then(() => {
        return getById(id)
    })
}
const createConstituency = (values) => {
    return knex('constituency').insert(values).then((result) => {
        return getById(result[0])
    })
}
const deleteConstituency = (id) => {
    return knex('constituency').where('constituency_id', id).del()
}
module.exports = {
    getById,
    getAll,
    updateConstituency,
    createConstituency,
    deleteConstituency
}