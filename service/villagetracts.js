const config = require('../knexfile')
const knex = require('knex')(config)

const getById = (id) => {
    return knex('village_tracts').where('village_tracts_id', id).first()
}

const getAll = () => {
    return knex.select('*').from('village_tracts')
}
const updateVillageTracts = (id, values) => {
    return knex('village_tracts').where('village_tracts_id', id).update(values).then(() => {
        return getById(id)
    })
}
const createVillageTracts = (values) => {
    return knex('village_tracts').insert(values).then((result) => {
        return getById(result[0])
    })
}
const deleteVillageTracts = (id) => {
    return knex('village_tracts').where('village_tracts_id', id).del()
}
module.exports = {
    getById,
    getAll,
    updateVillageTracts,
    createVillageTracts,
    deleteVillageTracts
}