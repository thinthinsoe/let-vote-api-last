const config = require('../knexfile')
const knex = require('knex')(config)

const getById = (id) => {
    return knex('district').where('district_id', id).first()
}

const getAll = () => {
    //return knex.select('*').from('district')
    return knex.select('district.*', 'state.state_region').from('district').leftJoin('state', 'district.state_id', 'state.state_id')
}

const updateDistrict = (id, values) => {
    return knex('district').where('district_id', id).update(values).then(() => {
        return getById(id)
    })
}

const createDistrict = (values) => {
    return knex('district').insert(values).then((result) => {
        return getById(result[0])
    })
}

const deleteDistrict = (id) => {
    return knex('district').where('district_id', id).del()
}

module.exports = {
    getById,
    getAll,
    updateDistrict,
    createDistrict,
    deleteDistrict
}