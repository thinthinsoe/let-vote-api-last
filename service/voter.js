const config = require('../knexfile')
const knex = require('knex')(config)

const getById = (id) => {
    return knex('voter').where('voter_id', id).first()
}

const getAll = () => {
    return knex.select('*').from('voter')
}
const updateVoter = (id, values) => {
    return knex('voter').where('voter_id', id).update(values).then(() => {
        return getById(id)
    })
}
const createVoter = (values) => {
    return knex('voter').insert(values).then((result) => {
        return getById(result[0])
    })
}
const deleteVoter = (id) => {
    return knex('voter').where('voter_id', id).del()
}
module.exports = {
    getById,
    getAll,
    updateVoter,
    createVoter,
    deleteVoter
}