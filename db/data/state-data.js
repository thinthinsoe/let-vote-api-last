const stateList = [
    { state_id: 1, state_code: 'MMR001', state_region: 'Kachin', remark: 'One of favorite things', created_by: 'Shwe', updated_by: 'Mi', status: 'Active' },
    { state_id: 2, state_code: 'MMR002', state_region: 'Kayah', remark: 'One of favorite things', created_by: 'Su', updated_by: 'Zin', status: 'Active' },
    { state_id: 3, state_code: 'MMR003', state_region: 'Kayin', remark: 'One of favorite things', created_by: 'SinSin', updated_by: 'Nu', status: 'Active' },
    { state_id: 4, state_code: 'MMR004', state_region: 'Chin', remark: 'One of favorite things', created_by: 'Win', updated_by: 'Hla', status: 'active' },
    { state_id: 5, state_code: 'MMR005', state_region: 'Sagaing', remark: 'One of favorite things', created_by: 'WinWin', updated_by: 'MoMo', status: 'Active' },
    { state_id: 6, state_code: 'MMR006', state_region: 'Tanintharyi', remark: 'One of favorite things', created_by: 'ShweSin', updated_by: 'Thu', status: 'Active' },
    { state_id: 7, state_code: 'MMR007', state_region: 'Bago', remark: 'One of favorite things', created_by: 'ShweShwe', updated_by: 'Kay', status: 'Active' },
    { state_id: 8, state_code: 'MMR009', state_region: 'Magway', remark: 'One of favorite things', created_by: 'SSW', updated_by: 'KayKay', status: 'Active' },
    { state_id: 9, state_code: 'MMR0010', state_region: 'Mandalay', remark: 'One of favorite things', created_by: 'PS', updated_by: 'PP', status: 'Active' },
    { state_id: 10, state_code: 'MMR0011', state_region: 'Mon', remark: 'One of favorite things', created_by: 'AA', updated_by: 'HH', status: 'Active' },
    { state_id: 11, state_code: 'MMR0012', state_region: 'Rakhine', remark: 'One of favorite things', created_by: 'Wai', updated_by: 'KK', status: 'Active' },
    { state_id: 12, state_code: 'MMR0013', state_region: 'Yangon', remark: 'One of favorite things', created_by: 'LaLa', updated_by: 'JJ', status: 'Active' },
    { state_id: 13, state_code: 'MMR0014', state_region: 'Shan', remark: 'One of favorite things', created_by: 'Oo', updated_by: 'Ei', status: 'Active' },
    { state_id: 14, state_code: 'MMR0017', state_region: 'Ayeyarwady', remark: 'One of favorite things', created_by: 'Htet', updated_by: 'Htwe', status: 'Active' }
]

module.exports = stateList;

