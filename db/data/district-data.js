const districtList = [
    { district_id: 1, state_id: 5, district_code: 'MMR005D003', district_name: 'Monywa', remark: 'one of favorit things', created_by: 'kayzin', updated_by: 'shweshwe', status: 'Active' },
    { district_id: 2, state_id: 5, district_code: 'MMR005D002', district_name: 'Shwebo', remark: 'one of favorit things', created_by: 'kayzin', updated_by: 'shweshwe', status: 'Active' },
    { district_id: 3, state_id: 5, district_code: 'MMR005D010', district_name: 'Kabanlu', remark: 'one of favorit things', created_by: 'kayzin', updated_by: 'shweshwe', status: 'Active' },
    { district_id: 4, state_id: 5, district_code: 'MMR005D004', district_name: 'Katha', remark: 'one of favorit things', created_by: 'kayzin', updated_by: 'shweshwe', status: 'Active' },
    { district_id: 5, state_id: 5, district_code: 'MMR005D007', district_name: 'Mawlaik', remark: 'one of favorit things', created_by: 'kayzin', updated_by: 'shweshwe', status: 'Active' },
    { district_id: 6, state_id: 5, district_code: 'MMR005D008', district_name: 'Hkamti', remark: 'one of favorit things', created_by: 'kayzin', updated_by: 'shweshwe', status: 'Active' },
    { district_id: 7, state_id: 5, district_code: 'MMR005D001', district_name: 'Sagaing', remark: 'one of favorit things', created_by: 'kayzin', updated_by: 'shweshwe', status: 'Active' },
    { district_id: 8, state_id: 5, district_code: 'MMR005D005', district_name: 'Kale', remark: 'one of favorit things', created_by: 'kayzin', updated_by: 'shwezin', status: 'Active' },
    { district_id: 9, state_id: 5, district_code: 'MMR005D006', district_name: 'Tamu', remark: 'one of favorit things', created_by: 'kayzin', updated_by: 'shweshwe', status: 'Active' },
    { district_id: 10, state_id: 5, district_code: 'MMR005D009', district_name: 'Yinmarbin', remark: 'one of favorit things', created_by: 'kayzin', updated_by: 'shweshwe', status: 'Active' }
  ]

module.exports = districtList;