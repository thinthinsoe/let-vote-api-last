const townList = [
    { town_id: 1, township_id: 1, town_code: 'MMR005012701', town_name: 'Monywa Town', remark: 'Fine', created_by: 'Phyoe', updated_by: 'Maw', status: 'Active' },
    { town_id: 2, township_id: 2, town_code: 'MMR005014701', town_name: 'Ayadaw Town', remark: 'Fine', created_by: 'Phyoe', updated_by: 'Maw', status: 'Active' },
    { town_id: 3, township_id: 3, town_code: 'MMR005013701', town_name: 'Budalin Town', remark: 'Fine', created_by: 'Phyoe', updated_by: 'Maw', status: 'Active' },
    { town_id: 4, township_id: 4, town_code: 'MMR005015701', town_name: 'Chang U', remark: 'Fine', created_by: 'Phyoe', updated_by: 'Maw', status: 'Active' },
    { town_id: 5, township_id: 5, town_code: 'MMR005004701', town_name: 'Shwebo Town', remark: 'Fine', created_by: 'Phyoe', updated_by: 'Maw', status: 'Active' },
    { town_id: 6, township_id: 5, town_code: 'MMR005004702', town_name: 'Kyauk Myaung Town', remark: 'Fine', created_by: 'Phyoe', updated_by: 'Maw', status: 'Active' },
    { town_id: 7, township_id: 6, town_code: 'MMR005005701', town_name: 'Khin_U Town', remark: 'Fine', created_by: 'Phyoe', updated_by: 'Maw', status: 'Active' },
    { town_id: 8, township_id: 7, town_code: 'MMR005010701', town_name: 'Tabayin Town', remark: 'Fine', created_by: 'Phyoe', updated_by: 'Maw', status: 'Active' },
    { town_id: 9, township_id: 8, town_code: 'MMR005011701', town_name: 'Taze Town', remark: 'Fine', created_by: 'Phyoe', updated_by: 'Maw', status: 'Active' },
    { town_id: 10, township_id: 9, town_code: 'MMR005006701', town_name: 'WetLet Town', remark: 'Fine', created_by: 'Phyoe', updated_by: 'Maw', status: 'Active' },
    { town_id: 11, township_id: 10, town_code: 'MMR005009701', town_name: 'Ye-U Town', remark: 'Fine', created_by: 'Phyoe', updated_by: 'Maw', status: 'Active' }  
  ]

  module.exports = townList;