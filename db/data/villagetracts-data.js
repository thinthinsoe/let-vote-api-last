const villagetractsList = [
    { village_tracts_id: 1, township_id: 1, village_tracts_code: 'MMR005012028', village_tracts_name: 'Aung Thar', remark: 'fav', created_by: 'tts', updated_by: 'hns', status:'active'},
    { village_tracts_id: 2, township_id: 1, village_tracts_code: 'MMR005012007', village_tracts_name: 'Bon Ma No', remark: 'fav', created_by: 'tts', updated_by: 'hns', status:'active'},
    { village_tracts_id: 3, township_id: 1, village_tracts_code: 'MMR005012056', village_tracts_name: 'Bu Ba', remark: 'fav', created_by: 'tts', updated_by: 'hns', status:'active'},
    { village_tracts_id: 4, township_id: 1, village_tracts_code: 'MMR005012006', village_tracts_name: 'Bu Taung Kan', remark: 'fav', created_by: 'tts', updated_by: 'hns', status:'active'},
    { village_tracts_id: 5, township_id: 1, village_tracts_code: 'MMR005012044', village_tracts_name: 'Dan Pin Te', remark: 'fav', created_by: 'tts', updated_by: 'hns', status:'active'},
    { village_tracts_id: 6, township_id: 1, village_tracts_code: 'MMR005012022', village_tracts_name: 'Et Taw', remark: 'fav', created_by: 'tts', updated_by: 'hns', status:'active'},
    { village_tracts_id: 7, township_id: 1, village_tracts_code: 'MMR005012043', village_tracts_name: 'Hpan Khar Kyin', remark: 'fav', created_by: 'tts', updated_by: 'hns', status:'active'},
    { village_tracts_id: 8, township_id: 1, village_tracts_code: 'MMR005012032', village_tracts_name: 'Hta Naung Taw', remark: 'fav', created_by: 'tts', updated_by: 'hns', status:'active'},
    { village_tracts_id: 9, township_id: 1, village_tracts_code: 'MMR005012020', village_tracts_name: 'Hta Naung Win', remark: 'fav', created_by: 'tts', updated_by: 'hns', status:'active'},
    { village_tracts_id: 10, township_id: 1, village_tracts_code: 'MMR005012026', village_tracts_name: 'In Taing', remark: 'fav', created_by: 'tts', updated_by: 'hns', status:'active'},
    
  ]
  

module.exports = villagetractsList;