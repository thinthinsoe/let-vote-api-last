const partyList = [
    { party_id: 1, party_name: 'National League for Democracy', description: 'NLD', remark: 'fav', created_by: 'tts', updated_by: 'hns', status: 'active' },
    { party_id: 2, party_name: 'Union Solidarity and Development Party', description: 'USDP', remark: 'fav', created_by: 'tts', updated_by: 'hns', status: 'active' },
    { party_id: 3, party_name: 'Arakan National Party', description: 'ANP', created_by: 'tts', remark: 'fav', updated_by: 'hns', status: 'active' },
    { party_id: 4, party_name: 'Shan Nationalities League for Democracy', description: 'SNLD', remark: 'fav', created_by: 'tts', updated_by: 'hns', status: 'active' },
    { party_id: 5, party_name: 'Zomi Congress for Democracy', description: 'ZCD', remark: 'fav', created_by: 'tts', updated_by: 'hns', status: 'active' },
    { party_id: 6, party_name: 'National Unity Party', description: 'NUP', remark: 'fav', created_by: 'tts', updated_by: 'hns', status: 'active' },
    { party_id: 7, party_name: 'Kachin State Democracy Party', description: 'KSDP', remark: 'fav', created_by: 'tts', updated_by: 'hns', status: 'active' }
  ]
module.exports = partyList;