
exports.up = function(knex) {
    return knex.schema.createTable('candidate', function (table) {
        table.increments('candidate_id').primary().notNullable();
        
        table.integer('party_id').unsigned().notNullable();
        table.foreign('party_id').references('party_id').inTable('party');
        
        table.string('candidate_name', 250).notNullable();
        table.string('image').notNullable();
        table.string('election_name', 250).notNullable();
        table.date('date_of_birth').notNullable();
        table.string('race', 250).notNullable();
        table.string('religion', 250).notNullable();
        table.string('nric_no', 30).notNullable();
        table.string('education', 250).notNullable();
        table.string('occupation', 250).notNullable();
        table.string('permanent_address', 250).notNullable();
        table.string('why_to_be_a_candidate', 250).notNullable();
        table.string('created_by', 250).notNullable();
        table.string('updated_by', 250).notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
        table.string('status', 50).notNullable();
        table.text('remark');
        })
  
  
};

exports.down = function(knex) {
    return knex.schema.dropTable('candidate')
};
