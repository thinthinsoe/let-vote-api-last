
exports.up = function(knex) {
    return knex.schema.createTable('party', function (table) {
        table.increments('party_id').primary().notNullable();
        table.string('party_name', 250).notNullable();
        table.string('description', 250).notNullable();
        table.string('created_by', 250).notNullable();
        table.string('updated_by', 250).notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
        table.string('status', 50).notNullable();
        table.text('remark');
        })
};

exports.down = function(knex) {
    return knex.schema.dropTable('party')
};
