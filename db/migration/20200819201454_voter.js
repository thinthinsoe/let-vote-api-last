
exports.up = function(knex) {
    return knex.schema.createTable('voter', function (table) {
        table.increments('voter_id').primary().notNullable();

        table.string('wards_code', 50);
        table.string('village_code', 50);

        table.string('voter_name', 50).notNullable();
        table.string('nric_no', 50).notNullable();
        table.date('date_of_birth').notNullable();
        table.string('race', 250).notNullable();
        table.string('religion', 30).notNullable();
        table.string('permanent_address', 250).notNullable();
        table.string('created_by', 250).notNullable();
        table.string('updated_by', 250).notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
        table.string('status', 50).notNullable();
        table.text('remark');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('voter')
};
