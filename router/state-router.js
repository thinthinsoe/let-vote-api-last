const Router = require('koa-router')
const stateService = require('../service/state')
const config = require('../knexfile')
const knex = require('knex')(config)

const router = new Router()

router.get('/state', async(ctx) => {
    const data = await stateService.getAll()
    ctx.body = data
})

router.get('/state/:id', async(ctx) => {
    const id = ctx.params.id
    const data = await stateService.getById(id)
    ctx.body = data
})

router.post('/state', async(ctx) => {
    let values = ctx.request.body
    let data = await stateService.createState(values)
    ctx.body = data

})

router.put('/state/:id', async(ctx) => {
    const id = ctx.params.id
    const values = ctx.request.body
    const data = await stateService.updateState(id, values)
    ctx.body = data

})

router.delete('/state/:id', async(ctx) => {
    const id = ctx.params.id
    const data = await stateService.deleteState(id)
    console.log(data, "data")
    if (data) {
        ctx.body = "DELETED"
    } else {
        ctx.body = "DATA NOT FOUND"
    }
})

module.exports = router